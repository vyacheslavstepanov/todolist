'use strict';

let path = require('path')
let webpack = require('webpack')

module.exports = {
	entry: [
		'webpack-hot-middleware/client',
		'babel-polyfill',
		'./src/index'
	],
	output: {
		path: path.join(__dirname, 'static'),
		publicPath: '/static/',
		filename: 'bundle.js'
	},	
	devtool: 'cheap-module-eval-source-map',
	plugins: [
		new webpack.optimize.OccurrenceOrderPlugin(),
		new webpack.HotModuleReplacementPlugin(),
		new webpack.NoEmitOnErrorsPlugin()
	],
	module: { //Обновлено
		loaders: [ //добавили babel-loader
			{
				loaders: ['babel-loader'],
				include: [
					path.resolve(__dirname, "src"),
				],
				test: /\.js$/,
			}
		]
	}
}